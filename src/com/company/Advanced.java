package com.company;

import java.util.Scanner;

public class Advanced {
    public static void main(String[] args) {
        String[] questions = {"投資資金は1,000万円以上", "長期間投資するつもり", "リスクは取りたくない",
                "株式以外にも投資したい", "業績や市況を分析したい"};
        int[] yes = {1, 3, -2, -1, -3};
        int[] no = {2, 4, 4, -2, -4};
        String[] result = {"①ポートフォリオタイプ", "②株式長期保有タイプ", "③分析研究タイプ", "④デイトレーダータイプ"};

        int i = 0;
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println(questions[i]);
            System.out.println("1: YES 2: NO");
            int input = scanner.nextInt();
            int next = 0;
            if (input == 1) {
                next = yes[i];
            } else if (input == 2) {
                next = no[i];
            }
            if (next < 0) {
                int res = next * -1 - 1;
                System.out.println(result[res]);
                break;
            }
            i = next;
        }
    }
}
